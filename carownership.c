#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAX 10

    typedef struct
    {
        char brand[20];
        char model[20];
        char license_plate[8];
        int year;
    }client;

client car[MAX];

void menu();
void CarListing();
void CarRegister();
void bubbleSort();
void listAll();
void listByYear();
void listCertainYear();
void listModel();
int counter;

int main()
{
    menu();
}

void menu()
{
    int op;

    do{
        printf("\n OPTIONS: ");
        printf("\n 1. REGISTER");
        printf("\n 2. LISTING OPTIONS");
        printf("\n 0. EXIT");
        printf("\n\n TYPE IN DESIRED OPTION: \n");
        scanf("%d%*c", &op);

        switch (op)
        {
        case 1:
            CarRegister();
            break;
        case 2:
            CarListing();
            break;
        case 0:
            printf("\nEXITING...\n");
            return 0;
        default:
            printf("\nERROR - INVALID OPTION\n");
            break;
        }
    } while (op > 0 || op <= 2);
}

void CarListing()
{
    int op2;

    do
    {
        printf("\n1. LIST ALL THE CARS");
        printf("\n2. LIST CARS BY YEAR");
        printf("\n3. LIST CARS ABOVE CERTAIN YEAR");
        printf("\n4. LIST CARS BY MODEL");
        printf("\n0. EXIT\n");
        scanf("%d%*c", &op2);

        switch (op2)
        {
        case 1:
            listAll();
            break;
        case 2: 
            listByYear();
            break;
        case 3:
            listCertainYear();
            break;
        case 4:
            listModel();
            break;
        case 0:
            printf("EXITING...\n\n");
            return 0;
        default:
            printf("ERROR - INVALID OPTION");
            break;
        }
    } while (op2 >= 0 || op2 <= 4);
    
}

void CarRegister()
{
    if(counter < MAX)
    {
        printf("BRAND: ");
        fflush(stdin);
        fgets(car[counter].brand,20,stdin);

        printf("MODEL: ");
        fflush(stdin);
        fgets(car[counter].model,20,stdin);

        printf("LICENSE PLATE: ");
        fflush(stdin);
        fgets(car[counter].license_plate,8,stdin);

        printf("YEAR OF FABRICATION: ");
        scanf("%d%*c", &car[counter].year);
        
        counter++;
        bubbleSort();
    }
    else
    {
        printf("ERROR - MAXIMUM NUMBER OF ENTRIES");
    }
}

void bubbleSort()
{
    int i;
    
    for(i = 0; i < counter; i++)
    {
        if (counter == 0)
        {
            break;
        }
        int pos;
        
        for (pos = 0; pos < i; pos++)
        {
            if(car[i].year < car[pos].year)
            {
                client aux;
                aux = car[pos];
                car[pos] = car[i];
                car[i] = aux;
            }
        }
    }
}

void listAll()
{
    int i;

    for(i = 0; i < counter; i++)
    {
        printf("\nBRAND: %s", car[i].brand);
        printf("\nMODEL: %s", car[i].model);
        printf("\nLICENSE PLATE: %s", car[i].license_plate);
        printf("\nYEAR OF FABRICATION: %d\n", car[i].year);
        printf("---------------------------\n");
    }
}

void listByYear()
{
    int i;
    int year;

    printf("\nTYPE IN THE DESIRED YEAR: ");
    scanf("%d%*c", &year);

    for(i = 0; i < counter; i++)
    {
        if(car[i].year == year)
        {
            printf("\nBRAND: %s", car[i].brand);
            printf("\nMODEL: %s", car[i].model);
            printf("\nLICENSE PLATE: %s", car[i].license_plate);
            printf("\nYEAR OF FABRICATION: %d\n", car[i].year);
            printf("---------------------------\n");
        }
    }
}

void listCertainYear()
{
    int i;
    int year;

    printf("\nTYPE IN DESIRED YEAR:");
    scanf("%d%*c", &year);

    for(i = 0; i < counter; i++)
    {
        if(car[i].year >= year)
        {
            printf("\nBRAND: %s", car[i].brand);
            printf("\nMODEL: %s", car[i].model);
            printf("\nLICENSE PLATE: %s", car[i].license_plate);
            printf("\nYEAR OF FABRICATION: %d\n", car[i].year);
            printf("---------------------------\n");
        }
    }
}

void listModel()
{
    int i, comp;
    char model[20];

    printf("\nTYPE IN DESIRED MODEL:\n");
    fflush(stdin);
    scanf("%s", &model);

    printf("DESIRED MODEL: %s\n\n", model);

    
    for(i = 0; i < counter; i++)
    {
        if(strcmp(model, car[i].model) == 0)
        {
            printf("\nBRAND: %s", car[i].brand);
            printf("\nMODEL: %s", car[i].model);
            printf("\nLICENSE PLATE: %s", car[i].license_plate);
            printf("\nYEAR OF FABRICATION: %d\n", car[i].year);
            printf("---------------------------\n");
        }
        else
        {
            printf("\nERROR - NO CAR FOUND");
            return 1;
        }
    }
}
